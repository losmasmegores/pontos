using m17;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Transforms;
using Unity.Mathematics;
using UnityEngine;

[BurstCompile]
public partial struct EntitiesSystem : ISystem
{
    private static bool Once, Once2;
    private static float perlingposition;

    public void OnUpdate(ref SystemState state)
    {
        int EntitiesToMake = 160000;
        int WidthLength;
        float Size = 0.04f;

        
        // Detectar si se ha presionado la tecla "Space" para borrar todos los puntos
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ClearDots(state);
        }
        <
        
        if (!Once)
        {

            CubeRef cubeRef = SystemAPI.GetSingleton<CubeRef>();

            for (int i = 0; i < EntitiesToMake / 5; i++)
            {
                JobHandle handle = new SpawnJob { cubeRef = cubeRef, CommandBuffer = GetCommandBuffer(state) }.ScheduleParallel(state.Dependency);
                handle.Complete();
            }

            Once = true;
            // state.Enabled = false;
        }
        if (!Once2)
        {
            EntityQuery query = state.EntityManager.CreateEntityQuery(typeof(LocalTransform));

            WidthLength = (int)math.sqrt(EntitiesToMake);

            int x = 0, z = 0;
            if(query.CalculateEntityCount() > EntitiesToMake)
            {
                foreach (var transform in SystemAPI.Query<RefRW<LocalTransform>>())
                {
                    transform.ValueRW.Scale = Size;

                    // Calcula el desplazamiento sinusoidal en el eje Y basado en el tiempo
                    float yOffset =
                        PerlinNoiseUtilities.CalculatePerlinNoise(Size, Size, 10, WidthLength, EntitiesToMake);

                    // Asigna la posición con el desplazamiento sinusoidal
                    transform.ValueRW.Position = new float3(x * Size, yOffset, z * Size);

                    if (x != WidthLength - 1) x++;
                    else
                    {
                        x = 0;
                        z++;
                    }
                }
                Once2 = true;
            }
        }

        // new MoveJob {ElapsedTime = (float)SystemAPI.Time.ElapsedTime}.ScheduleParallel();
        new MoveJob {ElapsedTime = perlingposition}.ScheduleParallel();
        perlingposition += 0.6f;
    }
    [BurstDiscard]
    EntityCommandBuffer.ParallelWriter GetCommandBuffer(SystemState state) 
    {
        return SystemAPI.GetSingleton<BeginSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(state.World.Unmanaged).AsParallelWriter();
    }
    private void ClearDots(SystemState state)
    {
        // Eliminar todos los puntos del mundo
        var commandBuffer = GetCommandBuffer(state);
        var query = state.EntityManager.CreateEntityQuery(typeof(LocalToWorld));
        var entities = query.ToEntityArray(Allocator.TempJob);

        // Iterar sobre todas las entidades y destruirlas
        for (int i = 0; i < entities.Length; i++)
        {
            commandBuffer.DestroyEntity(i, entities[i]);
        }

        // Liberar recursos y enviar los cambios al EntityManager
        entities.Dispose();
    }
    

    
}
[BurstCompile]
public partial struct SpawnJob : IJobEntity 
{
    public CubeRef cubeRef;

    public EntityCommandBuffer.ParallelWriter CommandBuffer;
    public void Execute([ChunkIndexInQuery] int Index) 
    {
        CommandBuffer.Instantiate(Index, cubeRef.Cube);
    }
}

public partial struct MoveJob : IJobEntity 
{
    public float ElapsedTime;

    public void Execute(EntitiesAspect aspect) 
    {
        aspect.MoveEntity(ElapsedTime);
    }
}

