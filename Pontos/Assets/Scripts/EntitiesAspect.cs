using m17;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using UnityEngine.UIElements;

public readonly partial struct EntitiesAspect : IAspect
{
    private readonly RefRW<LocalTransform> transform;
    private readonly RefRO<WaveSettings> waveSettings;

    public void MoveEntity(float perlingposition)
    {
        float y = 
        //     math.sin(ElapsedTime * waveSettings.ValueRO.Speed + (transform.ValueRO.Position.x + transform.ValueRO.Position.z) * waveSettings.ValueRO.Smoothness) * waveSettings.ValueRO.Height;
        PerlinNoiseUtilities.CalculatePerlinNoise(perlingposition+transform.ValueRO.Position.x,perlingposition+transform.ValueRO.Position.y, 8, 100, 100) * 10;
        transform.ValueRW.Position = new float3(transform.ValueRO.Position.x, y, transform.ValueRO.Position.z);
    }
}
